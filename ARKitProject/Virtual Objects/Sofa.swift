import Foundation

class Sofa: VirtualObject {
    
    override init() {
        super.init(modelName: "Sofa", fileExtension: "scn", thumbImageFilename: "vase", title: "Sofa")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
